# Kubernetes Authentication Demo #

Kubernetes has a lot of authentication mechanisms for verifying who is actually accessing cluster resources.   
For complete list check [Authentication Strategies list](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#authentication-strategies).
In this demo let's test the easiest one called [Static Password file](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#static-password-file)    

## Demo Prerequisites 

* Minikube kubernetes cluster
* Static password file called users.csv

Let's show the file and it's structure (password, username, userId) :    

    Macbooks-MacBook-Pro:workspace tomask79$ pwd
    /Users/tomask79/workspace
    Macbooks-MacBook-Pro:workspace tomask79$ cat users.csv 
    tomask123,tomask79,100
    
## Starting the Minikube cluster with Basic Authentication enabled    

First step is to mount password file **users.csv** located in the current directory into Minikube.    
Let's use /var/lib/localkube/certs/ directory for that which is shared between the minikube vm and the apiserver container.    

    Macbooks-MacBook-Pro:workspace tomask79$ minikube mount $(pwd)/:/var/lib/localkube/certs/mini
    Mounting /Users/tomask79/workspace/ into /var/lib/localkube/certs/mini on the minikube VM
    This daemon process needs to stay alive for the mount to still be accessible...

Let the process running as suggested and open another terminal and start the minikube with that file as Static Password resource    
for basic authentication.

    Macbooks-MacBook-Pro:workspace tomask79$ minikube start --extra-config=apiserver.basic-auth-file=/var/lib/localkube/certs/mini/users.csv --kubernetes-version=v1.10.0
    Starting local Kubernetes v1.10.0 cluster...
    Starting VM...
    Getting VM IP address...
    Moving files into cluster...
    Setting up certs...
    Connecting to cluster...
    Setting up kubeconfig...
    Starting cluster components...
    Kubectl is now configured to use the cluster.
    Loading cached images from config file.
    
Okay Minikube is up and running. To be sure, we should verify the basic properties.    

    Macbooks-MacBook-Pro:workspace tomask79$ minikube ip
    192.168.99.100
    Macbooks-MacBook-Pro:workspace tomask79$ minikube version
    minikube version: v0.28.2
    Macbooks-MacBook-Pro:workspace tomask79$ kubectl cluster-info
    Kubernetes master is running at https://192.168.99.100:8443
    KubeDNS is running at https://192.168.99.100:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
    
## Interacting with Kubernetes API through Basic Authentication   

For interacting with [Kubernetes API server](https://blog.openshift.com/kubernetes-deep-dive-api-server-part-1/)    
you've got basically two options:

* Direct calls via REST API
* Using of Kubectl which calls REST API eventually as well.

First let's try the first option and **get the pods running in the default namespace.**
To authenticate via Basic Authentication against API Server, doc says:
 
    When using basic authentication from an http client, the API server expects an Authorization    
    header with a value of Basic BASE64ENCODED(USER:PASSWORD).
    
So first let's create Base64 string of **incorrect credentials** (we've got user tomask79 configured only):

    Macbooks-MacBook-Pro:workspace tomask79$ echo -n baduser:badpassword | base64
    YmFkdXNlcjpiYWRwYXNzd29yZA==

Now use this Base64 string to get the pods running inside of default namespace:

    Macbooks-MacBook-Pro:workspace tomask79$ curl -H "Authorization: Basic YmFkdXNlcjpiYWRwYXNzd29yZA==" https://192.168.99.100:8443/api/v1/namespaces/default/pods -k
    {
      "kind": "Status",
      "apiVersion": "v1",
      "metadata": {
   
      },
      "status": "Failure",
      "message": "Unauthorized",
      "reason": "Unauthorized",
      "code": 401
    }
    
We received what we expected. In the next step let's prepare Base64 string of **correct credentials**

    Macbooks-MacBook-Pro:workspace tomask79$ cat users.csv 
    tomask123,tomask79,100
    Macbooks-MacBook-Pro:workspace tomask79$ echo -n tomask79:tomask123 | base64
    dG9tYXNrNzk6dG9tYXNrMTIz

and run getting of pods inside of default namespace again:

    Macbooks-MacBook-Pro:workspace tomask79$ curl -H "Authorization: Basic dG9tYXNrNzk6dG9tYXNrMTIz" https://192.168.99.100:8443/api/v1/namespaces/default/pods -k
    {
      "kind": "Status",
      "apiVersion": "v1",
      "metadata": {
    
      },
      "status": "Failure",
      "message": "pods is forbidden: User \"tomask79\" cannot list pods in the namespace \"default\"",
      "reason": "Forbidden",
      "details": {
        "kind": "pods"
      },
      "code": 403
	}
    
Hm, tomask79 user doesn't have a priviledge for viewing the pods inside of default namespace. That makes sense.    
To solve this problem we need [RoleBinding](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#rolebinding-and-clusterrolebinding) 
which is single namespaced and one of clusteroles. There are four default types of [clusterRoles](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#default-roles-and-role-bindings):

* cluster-admin
* admin
* edit
* view

We want user tomask79 just be able to view the pods, so let's give him **view** cluster role

    Macbooks-MacBook-Pro:workspace tomask79$ kubectl create rolebinding tomask79-view-binding-default --clusterrole=view --user=tomask79 --namespace=default
    rolebinding.rbac.authorization.k8s.io/tomask79-view-binding-default created
    
and run getting of pods from default namespace again and this time

    Macbooks-MacBook-Pro:workspace tomask79$ curl -H "Authorization: Basic dG9tYXNrNzk6dG9tYXNrMTIz" https://192.168.99.100:8443/api/v1/namespaces/default/pods -k
    {
      "kind": "PodList",
      "apiVersion": "v1",
      "metadata": {
        "selfLink": "/api/v1/namespaces/default/pods",
        "resourceVersion": "1054"
      },
      "items": []
    }
    
API Server returned the pods!

## Kubectl and multiple contexts with different users

Most of the time you want to use [Kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) for iteracting with API Server.
Kubectl through it's config allows you to [access multiple clusters](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/) by just switching the context.
Let's play with this idea and **create new kubectl context with the same cluster, but with newly registered tomask79 user**

First let's add new user to the current kubectl config:

    Macbooks-MacBook-Pro:workspace tomask79$ kubectl config set-credentials tomask79 --username=tomask79 --password=tomask123
    User "tomask79" set.
    
Nice, let's check the configuration that user has been added:

    Macbooks-MacBook-Pro:workspace tomask79$ kubectl config view
    apiVersion: v1
    clusters:
    - cluster:
        certificate-authority: /Users/tomask79/.minikube/ca.crt
        server: https://192.168.99.100:8443
      name: minikube
    contexts:
    - context:
        cluster: minikube
        user: minikube
      name: minikube
    current-context: minikube
    kind: Config
    preferences: {}
    users:
    - name: minikube
      user:
        client-certificate: /Users/tomask79/.minikube/client.crt
        client-key: /Users/tomask79/.minikube/client.key
    - name: tomask79
      user:
        password: tomask123
        username: tomask79
        
We want to be able to switch between users when accessing the same minikube cluster, so let's **add the context  
pointing to the minikube cluster but via tomask79 user**:

    Macbooks-MacBook-Pro:workspace tomask79$ kubectl config set-context tomask79-context --cluster=minikube --user=tomask79
    Context "tomask79-context" created.
    
Okay, let's switch into new context
    
    Macbooks-MacBook-Pro:workspace tomask79$ kubectl config use-context tomask79-context
    Switched to context "tomask79-context".
    
and show final configuration
    
    Macbooks-MacBook-Pro:workspace tomask79$ kubectl config view
    apiVersion: v1
    clusters:
    - cluster:
        certificate-authority: /Users/tomask79/.minikube/ca.crt
        server: https://192.168.99.100:8443
      name: minikube
     contexts:
     - context:
        cluster: minikube
        user: minikube
     name: minikube
     - context:
       cluster: minikube
       user: tomask79
     name: tomask79-context
    current-context: tomask79-context
    kind: Config
    preferences: {}
    users:
    - name: minikube
      user:
        client-certificate: /Users/tomask79/.minikube/client.crt
        client-key: /Users/tomask79/.minikube/client.key
    - name: tomask79
      user:
        password: tomask123
        username: tomask79
        
Now everything running from kubectl is going to be as "tomask79" user!

    Macbooks-MacBook-Pro:workspace tomask79$ kubectl get pods
    No resources found.
    
These are the pods from default namespace which we already granted to tomask79 user.    
Now let's try kube-public namespace:

    Macbooks-MacBook-Pro:workspace tomask79$ kubectl get -n kube-public pods
    No resources found.
    Error from server (Forbidden): pods is forbidden: User "tomask79" cannot list pods in the namespace "kube-public"

Kube-public namespace is still forbidden to tomask79 user. What if we try to create RoleBinding under his account ;)    

    Macbooks-MacBook-Pro:workspace tomask79$ kubectl create rolebinding tomask79-view-binding-public --clusterrole=view --user=tomask79 --namespace=kube-public
    Error from server (Forbidden): rolebindings.rbac.authorization.k8s.io is forbidden: User "tomask79" cannot create rolebindings.rbac.authorization.k8s.io in the namespace "kube-public"

Makes sense. So let's switch into default context, allow tomask79 user to access the kube-public namespace    
and switch back to tomask79 context to test it.

    Macbooks-MacBook-Pro:workspace tomask79$ kubectl config use-context minikube
    Switched to context "minikube".
    Macbooks-MacBook-Pro:workspace tomask79$ kubectl create rolebinding tomask79-view-binding-public --clusterrole=view --user=tomask79 --namespace=kube-public
    rolebinding.rbac.authorization.k8s.io/tomask79-view-binding-public created
    Macbooks-MacBook-Pro:workspace tomask79$ kubectl config use-context tomask79-context
    Switched to context "tomask79-context".
    Macbooks-MacBook-Pro:workspace tomask79$ kubectl get -n kube-public pods
    No resources found.

Okay if you liked my K8s authentication demo leave me a comment at twitter

Best regards

Tomas